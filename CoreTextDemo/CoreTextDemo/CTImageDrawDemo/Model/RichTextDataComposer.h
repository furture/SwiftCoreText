//
//  RichTextDataComposer.h
//  CoreTextDemo
//
//  Created by aron on 2018/7/12.
//  Copyright © 2018年 aron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RichTextData.h"

@interface RichTextDataComposer : NSObject

- (RichTextData *)richTextDataWithFrame:(CGRect)frame;

@end
