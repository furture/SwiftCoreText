//
//  CTImageDrawView.h
//  CoreTextDemo
//
//  Created by aron on 2018/7/12.
//  Copyright © 2018年 aron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RichTextData.h"

@interface CTImageDrawView : UIView

@property (nonatomic, strong) RichTextData *data;

@end
